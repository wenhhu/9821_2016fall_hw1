/*
 * main.cpp
 *
 *  Created on: Aug 27, 2016
 *      Author: wenhaohu
 */

#include <Eigen/Dense>
#include <Eigen/src/LU/PartialPivLU.h>
#include "Functions.h"
#include <fstream>
#include <iostream>

using Eigen::MatrixXd;
using namespace std;

int main()
{
	Eigen::VectorXd b(5);
	b << 1,2,3,4,5;
	Eigen::VectorXd e(5);
	e << 1e-5,1e-5,1e-5,1e-5,1e-5;
	MatrixXd read = ReadMatrix("data.csv");
//	cout << read << endl;
	auto result = lu_no_pivoting(read);
//	cout << read.determinant() << endl;
	cout << "L is:" << endl;
	cout << result.first << endl;
	cout << "U is:" << endl;
	cout << result.second << endl;
	cout << "Validating the LU decomposition:" << endl;
	cout << result.first * result.second << endl;
	auto x = backward_subst(result.second, forward_subst(result.first, b));
	cout << "Solution of linear system:" << endl;
	cout << x << endl;
	cout << "Validating the solution:" << endl;
	cout << read * (x + e) << endl;
	cout <<  read.lu().matrixLU() << endl;

}

